# CP4I Integration 2023.2 Hands on Labs

In these labs you will experience  Cloud Pak for Integration from an app developer's perspective . You will dive into the creation, deployment, and testing of new external APIs using the IBM API Connect Developer Toolkit. You will implement near-real-time transactional data replication to reporting databases with IBM Event Streams (IBM's Kafka offering) and see how to synchronize Salesforce data bidirectionally with your application using IBM App Connect. 

## Exercise 1 - Create, deploy and test a new API using the API Connect Developer Toolkit

In this lab you will create a new API using the OpenAPI definition of an existing RESTful web-service that  gets realtime stock quotes. You will then test the deployed API by deploying the *IBM Trader Lite* application which is a simple stock trading sample, written as a set of microservices. The app uses the API definition that you will create to get realtime stock quotes.

The architecture of the app is shown below:

   ![Architecture](images/architecture.png)

- **Tradr** is a Node.js UI for the portfolio service
- The **portfolio** microservice sits at the center of the application. This microservice:
    * persists trade data using JDBC to a MariaDB database
    * invokes the **stock-quote** service that invokes an API defined in API Connect in CP4I to get stock quotes
    * calls the **trade-history** service to store trade data in a PostgreSQL database that can be queried for reporting purposes.
    * calls the **trade-history** service to get aggregated historical trade data.


### Step 1: Launch the API Manager

1.1 Click on the desktop icon to start the Chrome browser.

1.2 Click on the Toolbar bookmark for the IBM CP4I UI 

   ![CP4I Platform UI](images/platform-ui.png)

1.3 Click through any browser warnings for self signed certificates

1.4 In the drop down with label *Log in with* select *Enterprise LDAP* and login with the credentials assigned to you for this lab (**Note:** your user name will be in the format **studentn**)

   ![CP4I UI Login](images/cp4i-login.png)

1.5 Launch the API Connect Manager by clicking on the **multitenant** instance in the **API lifecycle management** tile.

   ![Launch API Manager](images/launch-api-manager.png)

1.6 If prompted to select a user registry select the Common Services registry.


### Step 2: Import an  OpenAPI definition file into API Manager

2.1 Click on the **Develop APIs and Products tile**

   ![Develop APIs](images/api-manager.png)

2.2 Click **Add** and select **API** from the context menu

   ![Add API](images/add-api.png)

2.3 On the next screen select **Existing OpenAPI** under **Import** and then click **Next**.

   ![Existing OpenAPI](images/existing-api.png)

2.4 Now choose **stock-quote-api.yaml** from the desktop folder in local file system and click **Select** and then click **Next**.

   ![Choose File](images/choose-file.png)

2.5 Do not select **Activate API**. Click  **Next**

2.6 The API should be imported successfully as shown below. Click **Edit API**.

  [![](images/edit-api.png)](images/edit-api.png)

### Step 3: Configure the API

After importing the existing API, the first step is to configure basic security before exposing it to other developers. By creating a client key  you are able to identify the app using the services. Next, we will define the backend endpoints where the API is actually running. API Connect supports pointing to multiple backend endpoints to match your multiple build stage environments.

3.1 In the left navigation select  **Host**  and replace the hard coded endpoint address with `$(catalog.host)` to indicate that you want calls to the external API to go through API Connect.

   ![Catalog Host](images/catalog-host.png)

3.2 Click **Save**

3.3 In the Edit API screen click **Security Schemes(0)** in the left navigation

3.4 Click the **Add +** button on the right

   ![Security Scheme](images/security-scheme.png)

3.5 In the **Security Scheme Name(Key)** field, type `client-id`.

3.6 Under **Security Definition Type**, choose **apiKey**. 

3.6 Under **Key Type**, choose **client_id**. 

3.7 For **Located In** choose **header**.

3.8 Leave `X-IBM-Client-Id` as the **Variable Name**.  Your screen should now look like the image below.

   ![Complete Security Scheme](images/edit-api-complete.png)  

3.9 Click the **Create** button and then click **Save**.

3.10 Next you'll require use of the Client Id to access your API. In the left Navigation select **Security(0)** and then click on **Create a Security Requirement** 

   ![Create Secuirty Requirement](images/create-security-req.png)

3.11 Select the Security Scheme you just created and the click **Create**.

   ![Select Security Scheme](images/security-req.png)

3.12 Click **Save**

3.13 Next you'll define the endpoint for the external API. Select  the **Gateway** tab, expand **Properties**  in the left navigation.

3.14 Click on the **target-url** property.

3.15 Copy the following URL to the clipboard. 

   ```
      https://stocktrader.ibmc.buildlab.cloud
   ```

3.16 Click on the **Send Text** icon and then paste the URL to the text area. 

   ![Target URL](images/send-text-1.png)

3.17 Click on **Send Text** to paste it into the **Property Value** field:

   ![Target URL](images/target-url.png)

3.18 Click **Save** to complete the configuration.

### Step 4: Test the API

In the API designer, you have the ability to test the API immediately after creation.

4.1 On the left  Navigation, click **Policies**.

4.2 Click **invoke** in the flow designer. Note the window on the right with the configuration. The **invoke** node calls the **target-url** (ie the external service).

  [![](images/invoke.png)](images/invoke.png)

4.3 Modify the **URL** field to include the request path passed in by the caller as well by appending `$(request.path)` to the **URL**. When you're done the field should be set to:

  `$(target-url)$(request.path)`

   ![Invoke URL](images/invoke-edited.png)

4.4 Click **Save**

4.5 Toggle the **Offline** switch and then click on the **Test** tab

   ![Test tab](images/test-tab.png)

4.6 The **Request** should be prefilled with the GET request to  **/stock-quote/djia**.

4.7 Note that your **client-id** is prefilled for you.

4.8 Click **Send**.

   ![Invoke API](images/invoke-api.png)

4.9 If this is the first test of the API, you may  see a certificate exception. Simply click on the link provided. This will open a new tab and allow you to click through to accept the self signed certificate. **Note**: Stop when you get a `401` error code in the new tab.

   ![Cert Exception](images/cert-exception.png)


4.10 Go back to the previous tab and click **Send** again.

4.11 Now you should see a **Response** section with Status code `200 OK` and the **Body** displaying the details of the simulated *Dow Jones Industrial Average*.

   ![API Call response](images/response.png)

4.12 Next you'll launch a Text Editor to save the *Client Id* and *Gateway endpoint* so you can test your API from the TraderLite app. Click on **Activities** (top left) and then click on the Text Editor icon.

   ![Launch Editor](images/launch-editor.png)

> *Note*: You can toggle between the browser window and Text Editor window by clicking on their respective icons at the bottom of your screen.

   ![Switch Windows](images/switch-window.png)

4.13  In the browser window click on the *Endpoint* tab.  Copy the value of the  **api-gateway-service** URL and the **Client-Id** to your Text Editor  so it can be used in the Stock Trader application later (**Note:** this is a shortcut to the regular process of publishing the API and then subscribing to it as a consumer).

   ![Endpoint Client](images/endpoint-client-id.png)
  

### Step 5: Install the TraderLite app

5.1 In a separate browser tab go to the OpenShift console URL  by clicking the bookmark labelled **OpenShift UI**

5.2 Login with your *studentn* credentials 

5.3 If you're in the Developer view, select the dropdown to switch to the Administrator view
    
   ![Developer View](images/switch-to-admin.png)

5.4 Click on **Projects** in the left navigation and then click on your ***studentnn*** project in the list

   ![Select Project](images/select-traderlite-project.png)

5.5 Click on **Installed Operators** (in the **Operators** section) in the left navigation and then click on the **TraderLite Operator** in the list.

   ![Select Operator](images/select-traderlite-operator.png)

5.6 Click the **Create Instance** to start the installation of the TraderLite app.

   ![Create instance](images/traderlite-create-instance.png)

5.7 Name the instance *traderlite*  

5.8 Scroll down the  page to the **Stock Quote Microservice** and replace the **API Connect URL**  and **API Connect ClientId** with the **api-gateway-service** URL and the **Client-Id** you saved in the previous section. Click **Create**

   ![Create Values](images/traderlite-create-values.png)

5.9 In the left navigation select **Pods** (in the **Workloads** section) and then wait for all the TraderLite pods to have a status of **Running** and be in the **Ready** state.

> *Note: You will know the traderlite-xxxxx pods are  in a ready state when the `Ready` column shows `1/1`.*

   ![Pods Ready](images/traderlite-pods-ready.png)

### Step 6: Verify that the Trader Lite app is calling your API successfully

6.1 In your OpenShift console click on **Routes** (in the **Networking** section) in the left navigation and then click on the icon next to the url for the **tradr** app (the UI for TraderLite)

   ![Run Tradr](images/traderlite-run-tradr.png)

6.2 Log in using the username `stock` and the password `trader`

   ![Stock Trader Login](images/stock-trader-login.png)

6.3 If the simulated  DJIA summary has data then congratulations ! It means that the API you created in API Connect is working !

   ![Simulated DJIA](images/djia-success.png)

### Summary

Congratulations ! You successfully completed the following key steps in this lab:

* Created an API by importing an OpenAPI definition for an existing REST service.
* Configured a ClientID/API Key for security set up a proxy to the existing API.
* Tested the API in the API Connect developer toolkit.
* Deployed the Trader Lite app and configured it to use the API you created.
* Tested the Trader Lite app to make sure it successfully uses your API.

<div style="page-break-after: always;"></div>

## Exercise 2 - Using IBM MQ and IBM Event Streams for near realtime data replication

In this lab you will use IBM MQ and IBM Event Streams to replicate data from a transactional database to a reporting database. The pattern used allows for seamless horizontal scaling to minimize the latency between the time the transaction is committed to the transactional database and when it is available to be queried in the reporting database.

The architecture of the solution you will build is shown below:

![Architecture diagram](images/architecture2.png)

* The **portfolio** microservice sits at the center of the application. This microservice:

    * sends completed transactions to an IBM MQ queue.
    * calls the **trade-history** service to get aggregated historical trade data.

* The **Kafka Connect** source uses the Kafka Connect framework and an IBM MQ source to consume the transaction data from IBM MQ and sends it to a topic in IBM Event Streams. By scaling this service horizontally you can decrease the latency between the time the transaction is committed to the transactional database and when it is available to be queried in the reporting database,

* The **Kafka Connect** sink uses the Kafka Connect framework and a Mongodb sink to receive the transaction data from IBM Event Streams  and publishes it to the reporting database. By scaling this service horizontally you can decrease the latency between the time the transaction is committed to the transactional database and when it is available to be queried in the reporting database.

### Step 1: Uninstall the TraderLite  app
If you've completed the API Connect then you will have the app running already. For this lab it's easier to install the app from  scratch so you can use the OpenShift GUI environment (as opposed to editing the yaml file of an existing instance) to select all the options needed for this lab. If the app is *NOT* installed skip to **Step 2**.

1.1 Go to the OpenShift console of your workshop cluster. Select your  ***studentn*** project. In the navigation on the left select **Installed Operators** in the **Operators** section and select the **TraderLite Operator**

   ![TraderLite Operator](images/traderlite-operator.png)

1.2 Click on the **TraderLite app** tab

   ![TraderLite CRD](images/traderlite-crd.png)

1.3 Click on the 3 periods to the right of the existing TraderLite CRD and select **Delete TraderLite** from the context menu.

   ![Delete TraderLite](images/select-traderlite-crd2.png)

1.4 In the navigation area on the left select **Pods** in the **Workloads** section. You should see that all the *traderlite-xxxxx-yyyyy* pods except for the operator pod are terminated.  

### Step 2: Create a topic in the Event Streams Management Console

2.1 Go to the CP4I Platform UI by clicking on the **IBM CP4I UI** bookmark in the bookmark toolbar.

2.2 Click on the link to the Event Streams instance

   ![Launch Event Streams](images/nav-to-es.png)

2.3 If prompted to login select the **Enterprise LDAP** user registry and log in with your credentials.

2.4 Click on the **Create a topic** tile

   ![Create Topic](images/create-topic.png)

2.5 Use your username for the name of the topic. For example, if your username is `student5` then name the topic `student5`.  Click **Next**.

2.6 Leave the default for partitions and click **Next**.

2.7 Leave the default for message retention and click **Next**.

2.8 Leave the default for replicas and click **Create topic**.

2.9 You should see your new topic listed.

   ![New Topic](images/new-topic.png)

### Step 3: Add messaging components to the Trader Lite app

In this section you will install the TraderLite app to start storing transactions as MQ messages, without setting up the KafkaConnect part that will move the transactions out of MQ, into Event Streams and then into MongoDB. This demonstrates how MQ can serve as a reliable store and forward buffer especially during temporary network disruption.

3.1 Go to the OpenShift console of your workshop cluster. Select your  ***studentn*** project. 

3.2 Click on **Installed Operators** (in the **Operators** section) in the left navigation and then click on the **TraderLite Operator** in the list.

   ![Select Operator](images/select-traderlite-operator.png)

3.3 Click the **Create Instance** to start the installation of the TraderLite app.

   ![Create Instance](images/traderlite-create-instance.png)

3.4 Name the instance *traderlite*  

3.5 Set the following values:

   + Under **Kafka Access** select the **studentn** topic

   + Enable **KafkaIntegration**

   + Under **Mqaccess** select the **STUDENTN.QUEUE** queue that corresponds to your username.

   ![Create Values](images/traderlite-create-values2.png)

3.6 Scroll down and click **Create**

3.7 In the left navigation select **Pods** (in the **Workloads** section) and then wait for all the TraderLite pods to have a status of **Running** and be in the **Ready** state.
mkdocs 
> *Note: You will know the traderlite-xxxxx pods are  in a ready state when the `Ready` column shows `1/1`.*


### Step 4: Generate some test data with the TraderLite app

4.1 In your OpenShift console click on **Routes** in the left navigation under the **Networking** section and then click on the icon next to the url for the **tradr** app (the UI for TraderLite)

   ![Run Tradr](images/traderlite-run-tradr.png)

4.2 Log in using the username `stock` and the password `trader`

   ![Login into Stock Trader](images/stock-trader-login.png)

4.3 Click **Add Client** and fill in the form. You must use valid email and phone number formats to avoid validation errors.

   ![Add Client](images/new-client.png)

4.4 Click **Save**

4.5 Click on the **Portfolio ID** of the new client to see the details of the portfolio

   ![Portfolio Details](images/new-portfolio.png)

4.6 Buy some shares of 2 or 3 different companies and then sell a portion of one of the shares you just bought. To buy shares, click the `Buy Stock` button, then select a company and enter a share amount. To sell shares, click the `Sell stock` button, then select the company symbol and enter the number of shares to sell.

   ![Buy/sell shares](images/a-few-trades.png)

>**Note:** Your ROI will be wrong because we are not yet replicating the historical trade data that goes in to the calculation of the ROI.

### Step 5: View messages in IBM MQ

5.1 Go to the CP4I Platform UI by clicking on the **IBM CP4I UI** bookmark in the bookmark toolbar.

5.2 Click on the link to the MQ instance

   ![Launch MQ](images/nav-to-mq.png)

5.3 If prompted to login select the **Enterprise LDAP** user registry and log in with your credentials.

5.4 Click on the **Manage QMTRADER** tile

   ![QMTRADER QM](images/manage-qmtrader-tile.png)

5.5 Click on the **STUDENTN.QUEUE** queue that corresponds to your username.

   ![Select queue](images/trader-queue.png)

5.6 You should see messages for the trades you just executed. The number of messages in the queue will vary based on the number of buy/sell transactions you performed in the previous steps.

   ![View message](images/mq-messages.png)

5.7 Keep the browser tab with the MQ web interface open as you'll need it later in the lab.

### Step 6: Start Kafka Replication

In this section you will configure the TraderLite app to start moving the transaction data out of MQ, into Kafka and then into the MongoDB reporting database.

6.1 Go to the OpenShift console of your assigned cluster.  In the navigation on the left select **Installed Operators** and select the **TraderLite Operator**

6.2 Click on the **TraderLite app** tab

   ![Configure Traderlite](images/traderlite-crd.png)

6.3 Click on the 3 periods to the right of the existing TraderLite CRD and select **Edit TraderLite** from the context menu.

6.4 Scroll down to line 151 and set **Kafka Connect enabled** to  **true** (leave all the other values unchanged).

   ![Enable Kafka Connect](images/kafka-connect-enabled.png)

6.5 Click **Save**.

6.6 In the navigation on the left select **Installed Operators** and select the **Event Streams** operator. 

   ![Event Streams Operator](images/es-operator.png)

6.7 Click on the **All instances** tab, select **Current namespace only** and  wait for the *mq-source* and *mongodb-sink* connectors to be in the *Ready* state before continuing.

   ![Kafka Connect Status](images/kc-status.png)

6.8 Go back to the browser tab with the MQ Console and verify that all the messages have been consumed by the *mq-source* connector. *(Note: You may need to reload this browser tab to see that the messages have been consumed.)*

   ![Check MQ](images/mq-empty.png)

### Step 7: Verify transaction data was replicated to the Trade History database

7.1 Go to the OpenShift console of your workshop cluster.  In the navigation on the left select **Routes** in the **Networking** section.

7.2 Copy the link for the *trade-history* microservice and paste it into a new browser tab.

> **Note:** You will get a 404 (Not Found) message if you try to access this URL as is. This is because the *trade-history* microservice requires extra path information.

   ![Trade History](images/trade-history.png)

7.3 Append the string `/trades/1000` to the address you pasted - you should get back some JSON with the test transactions that you ran earlier.

   ![Get Trades](images/trade-history2.png)


### Step 8: Examine the messages sent to your Event Streams topic

8.1 Go to the CP4I Platform UI by clicking on the **IBM CP4I UI** bookmark in the bookmark toolbar.

8.2 Click on the link to the Event Streams instance (**Note:**)

   ![Launch Event Streams](images/nav-to-es.png)

8.3 If prompted to login select the **Enterprise LDAP** user registry and log in with your credentials.

8.4 Click on the topics icon

   ![Topic Icon](images/topics-icon.png)

8.5 Click on the topic that corresponds to your username.

   ![Topic Name](images/topic-name.png)

8.6 Select a message to see it's details

   ![View Message](images/message-details.png)

### Summary

Congratulations ! You successfully completed the following key steps in this exercise:

* Configured the Trader Lite app to use MQ
* Deploy Kafka Connect with IBM Event Streams
* Generated transactions in the Trader Lite app and verified that the data is being replicated via MQ and Event Streams
* Used the CP4I MQ and Event Streams GUI tools to view in transit messages

<div style="page-break-after: always;"></div>

## Demo - Sync Salesforce data using IBM App Connect Enterprise

Because of the time constraints for this session, this exercise is available as a pre-recorded demo that is accessible after the lab. Please note the link below and feel free to watch it at your convenience. 

In this demo you will see how to  create App Connect flows to sync  client data in the  **IBM Trader Lite** app to Salesforce. This will occur whenever a user of the  **IBM Trader Lite** app creates a portfolio for a new client or accesses the data of an existing client.

The architecture of the demo is shown below:

![Architecture diagram](images/architecture3.png)

The link to the demo is shown below. 

[https://ibm.biz/cp4i-sf-demo](https://ibm.biz/cp4i-sf-demo)

> **Note:** You can write down the link and view the demo later.

## Questions ?

Thank you ! We hope you enjoyed your lab experience. If you have any post lab questions feel free to reach out to us:

  * David Carew: carew@us.ibm.com
  * Eric Cambel: Eric.Cambel@ibm.com

